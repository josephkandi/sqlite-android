package za.peruzal.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {
	
	static final String DATABASE_NAME = "ContactsDB";
	public static final String TABLE_NAME = "contacts";
	public static final String COLUMN_KEY_ROWID = "_id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_EMAIL = "email";
	static final int DATABASE_VERSION = 1;
	static final String TAG = "DBHelper";
	static final String DATABASE_CREATE_SQL = String.format("CREATE TABLE %s(%s integer primary key autoincrement, %s text, %s text)", 
			TABLE_NAME, COLUMN_KEY_ROWID, COLUMN_NAME, COLUMN_EMAIL);

	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null	, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d(TAG, DATABASE_CREATE_SQL);
		try {
			db.execSQL(DATABASE_CREATE_SQL);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(TAG, "Might data, upgrading from " + oldVersion + " to " + newVersion);
		String sql = String.format("DROP TABLE IF EXISTS %s", TABLE_NAME);
		try {
			db.execSQL(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		onCreate(db);
		
	}

}
