package za.peruzal.database;

import android.app.Activity;
import android.app.ListActivity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.Menu;

public class MainActivity extends ListActivity {
	static final String TAG = "MainActivity";
	SQLiteDatabase db;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		DBHelper dbHelper = new DBHelper(this);
		
		//Get a writable database
		db =  dbHelper.getWritableDatabase();
		
		//Insert a record
		ContentValues values = new ContentValues();
		values.put(DBHelper.COLUMN_NAME, "Joseph Kanid");
		values.put(DBHelper.COLUMN_EMAIL, "joseph@peruzal.co.za");
		
		long id =  db.insert(DBHelper.TABLE_NAME, null, values);
		if (id == -1) {
			Log.d(TAG, "Error inserting ");
		}
		
		//Read from the table
		String[] columns = { DBHelper.COLUMN_KEY_ROWID, DBHelper.COLUMN_NAME, DBHelper.COLUMN_EMAIL };
		String orderBy = String.format("%s ASC", DBHelper.COLUMN_NAME);
		Cursor cursor = db.query(DBHelper.TABLE_NAME, columns, null, null, null, null, orderBy);
		
		//Move to the first record
		if (cursor.moveToFirst()) {
			do {
				String output = String.format("Name : %s\tEmail: %s",
						cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_NAME)),
						cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_EMAIL)));
				Log.d(TAG, output);
			} while (cursor.moveToNext());
		}
		
		//Update the record
		ContentValues update = new ContentValues();
		update.put(DBHelper.COLUMN_EMAIL, "joseph@apps4u.co.za");
		String whereClause = String.format("%s = '%s'", DBHelper.COLUMN_EMAIL, "joseph@peruzal.co.za");
		
		int rows = db.update(DBHelper.TABLE_NAME, update, whereClause, null);
		if (rows > 0) {
			Log.d(TAG, "Updated " + rows + " rows" );
		}
		
		//Delete the record
		whereClause = String.format("%s = %d ", dbHelper.COLUMN_KEY_ROWID, 1);
		
		rows = db.delete(DBHelper.TABLE_NAME, whereClause, null);
		
		if (rows > 0) {
			Log.d(TAG, "Deleted " + rows + " rows");
		}
		
		
		String[] from = { DBHelper.COLUMN_NAME, DBHelper.COLUMN_EMAIL };
		int[] to = { android.R.id.text1, android.R.id.text2 };
		//Instantiate an adapter
		
		SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, from, to);
		
		//Bind
		setListAdapter(adapter);

		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
